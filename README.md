# ESGPR
-----

This C++ library is an implementation of Exactly Sparse Gaussian Process Regression for batch state estimation accompanying the [Spatio-Temporal Multisensor Calibration Based on Gaussian Processes Moving Object Tracking](https://arxiv.org/abs/1904.04187) paper (submitted to IEEE Transactions on Robotics). For a more elaborate treat on Gaussian process regression for state estimation, the user is referred to Timothy Barfoot's [State Estimation for Robotics](http://asrl.utias.utoronto.ca/~tdb/bib/barfoot_ser17.pdf) book.

The library supports a user defined arbitrary system model. Current implementation provides the constant acceleration model described in the paper.

-----

#### DONE

* calculate posterior for linear systems
* provide support for asynchronous measurements
* calculate interpolation
* leverage Eigen's sparsity exploiting methods for faster computation

#### TO DO

* calculate posterior for nonlinear systems

-----

#### Installation

To build and install esgpr, inside its root folder run the following commands:
```
mkdir build && cd build
```
```
cmake..
```
```
sudo make install
```

After installing the toolbox, you can use it in your own project by adding the following lines to your project's CMakeLists.txt
```
find_library(ESGPR_LIB esgpr)
```
```
TARGET_LINK_LIBRARIES(your_project "${ESGPR_LIB}")
```

To test the toolbox within your project, you need to include it:
```
#include <esgpr/core/gp_regression.h>
```
You can then create an esgpr object with following line:
```
GPReg<ConstAcc3> example_regression($number_of_measurements);
```
The template argument defines model, which is in our case constant acceleration model with linear measurement of position (9 states, 3 measured variables) but can be defined by the user.

-----

#### Citing

If you use ESGPR in an academic context, please cite the following publication:

```
@article{Persic18preprint,
  author    = {Juraj Persic and Luka Petrovic and Ivan Markovic and Ivan Petrovic},
  title     = {Spatio-Temporal Multisensor Calibration Based on Gaussian Processes Moving Object Tracking},
  year      = {2019},
  url       = {https://arxiv.org/abs/1904.04187}
}
```

-----

#### Contributors
Luka Petrović, Juraj Peršić and Demijan Jurić who are with the [Laboratory for Autonomous Systems and Mobile Robotics (LAMOR)](https://lamor.fer.hr).

This work has been supported by the European Regional Development Fund under the project System for increased driving safety in public urban rail traffic (SafeTRAM). This research has also been carried out within the activities of the Centre of Research Excellence for Data Science and Cooperative Systems supported by the Ministry of Science and Education of the Republic of Croatia.

-----

#### License

ESGPR is released under the GPLv3 license, reproduced in the file [LICENSE.txt](https://bitbucket.org/unizg-fer-lamor/esgpr/src/master/LICENSE.txt) in the directory.