/**
 *  This file is a part of ESGPR.
 *
 *  Copyright (C) 2018 Luka Petrovic, University of Zagreb Faculty of Electrical
 Engineering and Computing

 *  ESGPR is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ESGPR_CONST_ACC3_H
#define ESGPR_CONST_ACC3_H

#include <eigen3/Eigen/Dense>
#include <math.h>

namespace esgpr {
class ConstAcc3 {
private:
  Eigen::Matrix<double, 3, 9> c_k_; // measurement matrix
  Eigen::Matrix<double, 3, 3> r_k_; // single measurement covariance
  Eigen::Matrix<double, 3, 3> q_c_; // power spectral density matrix
  Eigen::Matrix<double, 9, 1> x_0_; // state at the time t_0
  Eigen::Matrix<double, 9, 9> p_0_; // state covariance at t_0

public:
  ConstAcc3();
  // state transient matrix between time instants t and s
  void getPhi(Eigen::MatrixXd *phi, double t, double s);
  // noise propagation matrix
  void getQk(Eigen::MatrixXd *q_k, double t, double s);
  void getQkinv(Eigen::MatrixXd *q_k_inv, double t, double s);

  void getCk(Eigen::MatrixXd *c_k);
  void getRk(Eigen::MatrixXd *r_k);
  void getQc(Eigen::MatrixXd *q_c);
  void getX0(Eigen::MatrixXd *x_0);
  void getP0(Eigen::MatrixXd *p_0);
  int getS();
  int getM();

  void setRk(Eigen::MatrixXd &r_k);
  void setQc(Eigen::MatrixXd &q_c);
  void setX0(Eigen::MatrixXd &x_0);
  void setP0(Eigen::MatrixXd &P_0);
};
} // namsespace esgpr end

#endif