/**
 *  This file is a part of ESGPR.
 *
 *  Copyright (C) 2018 Luka Petrovic, University of Zagreb Faculty of Electrical
 Engineering and Computing

 *  ESGPR is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "constant_acceleration3.h"

namespace esgpr{
ConstAcc3::ConstAcc3() {
  // parameters currently hard-coded for comparison with matlab toolbox
  Eigen::Matrix<double, 3, 3> eye3;
  eye3 << 1, 0, 0, 0, 1, 0, 0, 0, 1;
  c_k_ << eye3, 0 * eye3, 0 * eye3;
  r_k_ << eye3 * 0.001;
  q_c_ << eye3;
  x_0_ << 0, 0, 0, 0, 0, 0, 0, 0, 0;
  p_0_ << 500 * Eigen::Matrix<double, 9, 9>::Identity();
}

void ConstAcc3::getPhi(Eigen::MatrixXd *phi, double t, double s) {
  Eigen::Matrix<double, 3, 3> eye3;
  eye3 << 1, 0, 0, 0, 1, 0, 0, 0, 1;

  // clang-format off
  *phi << eye3, (t - s) * eye3, 0.5 * (t - s) * (t - s) * eye3,
      0 * eye3, 1 * eye3, (t - s) * eye3,
      0 * eye3, 0 * eye3, 1 * eye3;
  // clang-format on
};

void ConstAcc3::getCk(Eigen::MatrixXd *c_k) { *c_k = c_k_; };

void ConstAcc3::getRk(Eigen::MatrixXd *r_k) { *r_k = r_k_; };

void ConstAcc3::getQc(Eigen::MatrixXd *q_c) { *q_c = q_c_; };

void ConstAcc3::getQk(Eigen::MatrixXd *q_k, double t, double s) {
  // clang-format off
  *q_k << std::pow((t-s),5)*q_c_/20,
          std::pow((t-s),4)*q_c_/8,
          std::pow((t-s),3)*q_c_/6,
          std::pow((t-s),4)*q_c_/8,
          std::pow((t-s),3)*q_c_/3,
          std::pow((t-s),2)*q_c_/2,
          std::pow((t-s),3)*q_c_/6,
          std::pow((t-s),2)*q_c_/2,
          (t-s)*q_c_;
  // clang-format on
};

void ConstAcc3::getQkinv(Eigen::MatrixXd *q_k_inv, double t, double s) {
  Eigen::Matrix<double, 3, 3> q_c_inv;
  q_c_inv = q_c_.inverse();

  // clang-format off
  *q_k_inv << 720 * std::pow((t - s), -5) * q_c_inv,
             -360 * std::pow((t - s), -4) * q_c_inv,
               60 * std::pow((t - s), -3) * q_c_inv,
             -360 * std::pow((t - s), -4) * q_c_inv,
              192 * std::pow((t - s), -3) * q_c_inv,
              -36 * std::pow((t - s), -2) * q_c_inv,
               60 * std::pow((t - s), -3) * q_c_inv,
              -36 * std::pow((t - s), -2) * q_c_inv,
                9 * std::pow((t - s), -1) * q_c_inv;
  // clang-format on
};

void ConstAcc3::getX0(Eigen::MatrixXd *x_0) { *x_0 = x_0_; };

void ConstAcc3::getP0(Eigen::MatrixXd *p_0) { *p_0 = p_0_; };

int ConstAcc3::getS() { return c_k_.cols(); };

int ConstAcc3::getM() { return c_k_.rows(); };

void ConstAcc3::setRk(Eigen::MatrixXd &r_k) { r_k_ = r_k; };

void ConstAcc3::setQc(Eigen::MatrixXd &q_c) { q_c_ = q_c; };

void ConstAcc3::setX0(Eigen::MatrixXd &x_0) { x_0_ = x_0; };

void ConstAcc3::setP0(Eigen::MatrixXd &p_0) { p_0_ = p_0; };

} // namsespace esgpr end
