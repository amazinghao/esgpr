/**
 *  This file is a part of ESGPR.
 *
 *  Copyright (C) 2018 Luka Petrovic, University of Zagreb Faculty of Electrical
 Engineering and Computing

 *  ESGPR is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ESGPR_GP_REG_H
#define ESGPR_GP_REG_H
#include <eigen3/Eigen/Sparse>

namespace esgpr {

/* T - class containing the system model, N - number of measurements,
 S - number of system states, M - number of measured variables
 */
template<class T>

class GPReg {
 private:
  int count_measurements_;

 public:
  // these variables will become private after debugging
  Eigen::SparseMatrix<double> b_A_;
  Eigen::SparseMatrix<double> b_A_inv_;
  Eigen::SparseMatrix<double> b_C_;
  Eigen::SparseMatrix<double> b_R_;
  Eigen::SparseMatrix<double> b_R_inv_;
  Eigen::SparseMatrix<double> b_Q_;
  Eigen::SparseMatrix<double> b_Q_inv_;

  Eigen::MatrixXd x_pr_;
  Eigen::MatrixXd y_;
  Eigen::MatrixXd x_po_;
  Eigen::SparseMatrix<double> b_P_pr_;
  Eigen::SparseMatrix<double> b_P_pr_inv_;
  Eigen::SparseMatrix<double> b_P_po_;

  Eigen::MatrixXd timestamp_;
  double measurement_frequency_;
  int S_;
  int M_;
  int N_;

  // this variable will stay public
  T model_;

  GPReg() {
    N_ = 0;
    S_ = model_.getS();
    M_ = model_.getM();
    count_measurements_ = 0;
    y_ = Eigen::MatrixXd::Zero(M_ * N_, 1);
    // default timestamp is equivalent to 20Hz synchronous measurements
    measurement_frequency_ = 0.05;
    timestamp_ = Eigen::MatrixXd::Zero(N_, 1);
    for (double i = 0; i < N_; i++) {
      timestamp_(i) = measurement_frequency_ * i;
    }
  };

  GPReg(int N) {
    N_ = N;
    S_ = model_.getS();
    M_ = model_.getM();
    count_measurements_ = 0;
    y_ = Eigen::MatrixXd::Zero(M_ * N_, 1);
    // default timestamp is equivalent to 20Hz synchronous measurements
    measurement_frequency_ = 0.05;
    timestamp_ = Eigen::MatrixXd::Zero(N_, 1);
    for (double i = 0; i < N_; i++) {
      timestamp_(i) = measurement_frequency_ * i;
    }
  };

  void setN(int N) {
    N_ = N;
    count_measurements_ = 0;
    y_ = Eigen::MatrixXd::Zero(M_ * N_, 1);
    timestamp_ = Eigen::MatrixXd::Zero(N_, 1);
  }

  double getMeasurementTime(const int index) {
    return (double) timestamp_.coeff(index, 0);
  }

  void constructA() {
    // follows eq. (3.168) in Barfoot's book

    Eigen::MatrixXd phi(S_, S_);
    Eigen::MatrixXd b_A(S_ * N_, S_ * N_);
    b_A = Eigen::MatrixXd::Identity(S_ * N_, S_ * N_);

    for (double i = 1; i <= N_; i++) {
      for (double j = 0; j <= i - 1; j++) {
        model_.getPhi(&phi, timestamp_(i - 1), timestamp_(j));
        b_A.block((i - 1) * S_, j * S_, S_, S_) = phi;
      }
    }
    b_A_ = b_A.sparseView();
  };

  void constructAinv() {
    // follows eq. (10) in the paper
    Eigen::MatrixXd phi(S_, S_);
    Eigen::SparseMatrix<double> b_A_inv(S_ * N_, S_ * N_);

    std::vector<Eigen::Triplet<double>> tripletList;
    tripletList.reserve(N_ * S_ * S_ + N_);

    for (double j = 0; j < N_ * S_; j++) {
      tripletList.push_back(Eigen::Triplet<double>(j, j, 1));
    }

    for (double i = 1; i < N_; i++) {
      model_.getPhi(&phi, timestamp_(i), timestamp_(i - 1));
      for (int k = 0; k < S_; k++)
        for (int l = 0; l < S_; l++)
          tripletList.push_back(Eigen::Triplet<double>(i * S_ + k, (i - 1) * S_ + l, -1 * phi(k, l)));
    }
    b_A_inv.setFromTriplets(tripletList.begin(), tripletList.end());

    b_A_inv_ = b_A_inv;
  };

  void constructC() {
    // block-diagonal batch matrix of the measurement model

    Eigen::MatrixXd c_k(M_, S_);
    Eigen::SparseMatrix<double> b_C(M_ * N_, S_ * N_);
    model_.getCk(&c_k);

    std::vector<Eigen::Triplet<double>> tripletList;
    tripletList.reserve(N_ * S_ * M_);
    for (int i = 0; i < N_; i++) {
      for (int k = 0; k < M_; k++)
        for (int l = 0; l < S_; l++)
          tripletList.push_back(Eigen::Triplet<double>(i * M_ + k, i * S_ + l, c_k(k, l)));
    }
    b_C.setFromTriplets(tripletList.begin(), tripletList.end());

    b_C_ = b_C;
  };

  void constructR() {
    // block-diagonal batch matrix of the Gaussian measurement noise

    Eigen::MatrixXd r_k(M_, M_);
    Eigen::MatrixXd b_R(M_ * N_, M_ * N_);
    b_R = Eigen::MatrixXd::Zero(M_ * N_, M_ * N_);
    model_.getRk(&r_k);

    for (int i = 0; i < N_; i++) {
      b_R.block(i * M_, i * M_, M_, M_) = r_k;
    }
    b_R_ = b_R.sparseView();
  }

  void constructRinv() {
    // inverse block-diagonal batch matrix of the Gaussian measurement noise
    Eigen::MatrixXd r_k(M_, M_);
    Eigen::SparseMatrix<double> b_R_inv(M_ * N_, M_ * N_);
    model_.getRk(&r_k);
    r_k = r_k.inverse();

    std::vector<Eigen::Triplet<double>> tripletList;
    tripletList.reserve(N_ * S_ * M_);
    for (int i = 0; i < N_; i++) {
      for (int k = 0; k < M_; k++)
        for (int l = 0; l < M_; l++)
          tripletList.push_back(Eigen::Triplet<double>(i * M_ + k, i * M_ + l, r_k(k, l)));
    }
    b_R_inv.setFromTriplets(tripletList.begin(), tripletList.end());

    b_R_inv_ = b_R_inv;
  }

  void constructQ() {
    // follows eq. (3.183) in Barfoot's book, block-diagonal matrix

    Eigen::MatrixXd q_k(S_, S_);
    Eigen::MatrixXd b_Q(S_ * N_, S_ * N_);
    b_Q = Eigen::MatrixXd::Zero(S_ * N_, S_ * N_);

    Eigen::MatrixXd p_0(S_, S_);
    model_.getP0(&p_0);
    b_Q.block(0, 0, S_, S_) = p_0;

    for (double i = 1; i < N_; i++) {
      model_.getQk(&q_k, timestamp_(i), timestamp_(i - 1));
      b_Q.block(i * S_, i * S_, S_, S_) = q_k;
    }
    b_Q_ = b_Q.sparseView();
  }

  void constructQinv() {
    // follows eq. (11) in the paper
    Eigen::MatrixXd q_k(S_, S_);
    Eigen::SparseMatrix<double> b_Q_inv(S_ * N_, S_ * N_);

    std::vector<Eigen::Triplet<double>> tripletList;
    tripletList.reserve(N_ * S_ * S_);
    Eigen::MatrixXd p_0(S_, S_);
    model_.getP0(&p_0);
    p_0 = p_0.inverse();
    for (int k = 0; k < S_; k++)
      for (int l = 0; l < S_; l++)
        tripletList.push_back(Eigen::Triplet<double>(k, l, p_0(k, l)));

    for (double i = 1; i < N_; i++) {
      model_.getQkinv(&q_k, timestamp_(i), timestamp_(i - 1));
      for (int k = 0; k < S_; k++)
        for (int l = 0; l < S_; l++)
          tripletList.push_back(Eigen::Triplet<double>(i * S_ + k, i * S_ + l, q_k(k, l)));
    }
    b_Q_inv.setFromTriplets(tripletList.begin(), tripletList.end());

    b_Q_inv_ = b_Q_inv;
  }

  void constructPprior() {
    // follows eq. (3.181) in Barfoot's book

    b_P_pr_ = b_A_ * b_Q_ * b_A_.transpose();
  };

  void constructPpriorInv() {
    // follows eq. (3.184) in Barfoot's book

    b_P_pr_inv_ = b_A_inv_.transpose() * b_Q_inv_ * b_A_inv_;
  };

  void constructXprior() {
    // follows eq. (7) in the paper

    Eigen::MatrixXd phi(S_, S_);
    Eigen::MatrixXd x_0(S_, 1);
    model_.getX0(&x_0);
    x_pr_ = Eigen::MatrixXd::Zero(S_ * N_, 1);

    for (double i = 0; i < N_; i++) {
      model_.getPhi(&phi, timestamp_(i), 0);
      x_pr_.block(i * S_, 0, S_, 1) = phi * x_0;
    }
  };

  void calculateXposterior() {
    // solves linear system in eq. (4) in the paper

    Eigen::SimplicialLDLT<Eigen::SparseMatrix<double>> sparseSolver(
        b_P_pr_inv_ + b_C_.transpose() * b_R_inv_ * b_C_);
    x_po_ = sparseSolver.solve(b_P_pr_inv_ * x_pr_ +
        b_C_.transpose() * b_R_inv_ * y_);
  }

  void calculatePposterior() {
    // posterior covariance defined in eq. (3) in the paper

    Eigen::SparseMatrix<double> I(S_ * N_, S_ * N_);
    I.setIdentity();

    Eigen::SimplicialLDLT<Eigen::SparseMatrix<double>> sparseSolver(
        b_P_pr_inv_ + b_C_.transpose() * b_R_inv_ * b_C_);
    b_P_po_ = sparseSolver.solve(I);
  }

  void interpolate(Eigen::MatrixXd *x_tau_po, double tau, int k1, int k2) {

    // implements GP interpolation defined by eqs. (13)-(15) in the paper

    double t1 = timestamp_(k1);
    double t2 = timestamp_(k2);

    // get all required matrices
    Eigen::MatrixXd q_tau(S_, S_);
    Eigen::MatrixXd q_t2_inv(S_, S_);
    Eigen::MatrixXd phi_t2_tau(S_, S_);
    Eigen::MatrixXd phi_tau_t1(S_, S_);
    Eigen::MatrixXd phi_t2_t1(S_, S_);
    model_.getQk(&q_tau, tau, t1);
    model_.getQkinv(&q_t2_inv, t2, t1);
    model_.getPhi(&phi_t2_tau, t2, tau);
    model_.getPhi(&phi_tau_t1, tau, t1);
    model_.getPhi(&phi_t2_t1, t2, t1);

    // calculate lambda and psi according to eqs. (14) and (15) in the paper
    Eigen::MatrixXd psi_tau(S_, S_);
    Eigen::MatrixXd lambda_tau(S_, S_);
    Eigen::MatrixXd x_tau_pr(S_, 1);
    psi_tau = q_tau * phi_t2_tau.transpose() * q_t2_inv;
    lambda_tau = phi_tau_t1 - psi_tau * phi_t2_t1;
    x_tau_pr = phi_tau_t1 * x_pr_.block(k1 * S_, 0, S_, 1);

    // calculate interpolated state according to (13) in the paper
    // clang-format off
    *x_tau_po =
        x_tau_pr +
            lambda_tau *
                (x_po_.block(k1 * S_, 0, S_, 1) - x_pr_.block(k1 * S_, 0, S_, 1)) +
            psi_tau *
                (x_po_.block(k2 * S_, 0, S_, 1) - x_pr_.block(k2 * S_, 0, S_, 1));
    // clang-format on
  }

  void getPosteriorState(Eigen::MatrixXd *state, const int k) { // returns posterior state at k-th measurement time
    *state = x_po_.block(k * S_, 0, S_, 1);
  }

  void addMeasurement(const Eigen::MatrixXd &y) {
    if (count_measurements_ < N_) {
      y_.block(M_ * count_measurements_, 0, M_, 1) = y;
      count_measurements_++;
    }
  };

  void addMeasurement(const Eigen::MatrixXd &y, double time) {
    if (count_measurements_ < N_) {
      y_.block(M_ * count_measurements_, 0, M_, 1) = y;
      timestamp_(count_measurements_) = time;
      count_measurements_++;
    }
  };
};
} // namespace esgpr
#endif
